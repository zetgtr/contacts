<?php

namespace Contact\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>config('contact.database_id'),'name'=>config('contact.title'),'position'=>'left','logo'=>'fal fa-id-card-alt','controller'=>'Contact\Http\Controllers\ContactController','url'=>'contacts','parent'=>5, 'order'=>4],
        ];
    }
}
