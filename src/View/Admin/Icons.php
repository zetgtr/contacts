<?php

namespace Map\View\Admin;

use Illuminate\View\Component;
use Map\Models\MapIcon;
use Map\Models\MapSettings;

class Icons extends Component
{

    public function __construct()
    {
        $this->icons = MapIcon::get();
    }


    public function render()
    {
        return view('admin::components.type.icons',['icons'=>$this->icons]);
    }
}
