const {YMapHintContext} = await ymaps3.import('@yandex/ymaps3-hint@0.0.1');
class HintWindow extends ymaps3.YMapComplexEntity {

    // Method for create a DOM control element
    _createElement() {
        const windowElement = document.createElement('div');
        windowElement.className = 'hintWindow';
        return windowElement;
    }

    // Callback method triggered on hint context change, responsible for updating the text in the hint window
    _searchContextListener() {
        this._element.innerHTML = this._consumeContext(YMapHintContext)?.hint;
    }

    // Method for attaching the control to the admin
    _onAttach() {
        this._element = this._createElement();
        this._unwatchSearchContext = this._watchContext(YMapHintContext, this._searchContextListener.bind(this));
        this._detachDom = ymaps3.useDomContext(this, this._element, this._element);
    }

    // Method for detaching control from the admin
    _onDetach() {
        this._unwatchSearchContext();
        this._detachDom();
    }
}

export default HintWindow
