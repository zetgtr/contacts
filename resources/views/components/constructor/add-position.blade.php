
<template id="add_position">
    <div class="row">
        <div class="col-12">
            <div class="">
                <div>
                    <h3 class="card-title"><span>Добавление точки</span><button type="button" class="btn-close"><i class="fas fa-times"></i></button></h3>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Заголовок</label>
                <input type="text" class="form-control" name="header">
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Описание - <a class="btn btn-sm edit-role modal-effect btn-primary tinymce badge">tinymce</a> </label>
                <textarea type="text" id="description" name="description" autocomplete="off" class="form-control" ></textarea>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Адрес</label>
                <textarea type="text" id="address" rows="4" name="address" autocomplete="off" class="form-control" ></textarea>
                <div class="container_address"></div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Категория</label>
                <select name="category" class="form-select form-control">
                    <option value="0">-- Выберете --</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Тип</label>
                <select name="types" id="types" class="form-select form-control">
                    <option value="0">-- Выберете --</option>
                    @foreach($types as $type)
                        <option data-img="{{ $type->icons->url }}" value="{{ $type->id }}">{{ $type->title }}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class="col-12">
            <button type="button" class="btn btn-sm btn-success modal-save">Добавить</button>
            <button type="button" class="btn btn-sm btn-danger d-none modal-danger">Удалить</button>
        </div>
    </div>
</template>
<x-map::constructor.modal />
<template id="address_template">
    <div class="container_item">
        <div style=""></div>
        <span></span>
    </div>
</template>

