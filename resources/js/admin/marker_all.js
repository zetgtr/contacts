class MarkerAll {
    constructor(map,admin) {
        this.map = map
        this.containerInfo = document.querySelector('.container_info')
        this.containerMap = document.querySelector('.container_map')
        this.admin = admin
    }

    open(){

        this.template = document.querySelector('#marker_template').content.children[0].cloneNode(true)
        this.template.querySelector('.btn-close').addEventListener('click',this.close.bind(this))
        const withMap = this.containerMap.offsetWidth
        if(!window.openCheck){
            this.containerMap.style.width = (withMap - 300) + "px"
            this.containerInfo.style.width = "300px"
            this.containerInfo.style.marginRight = "20px"
        }
        this.containerInfo.innerHTML = ''

        const container = this.template.querySelector('.container_marker_all')
        container.innerHTML = ''
        this.admin.addPosition.markers.forEach(el=>{
            let dataMarkers = JSON.parse(this.admin.addPosition.markersNode.value)
            const item = dataMarkers.filter(item=>item.id === el.id)[0]
            const itemNode = document.createElement('div')
            itemNode.classList.add('category_item')
            itemNode.dataset.id = item.id
            itemNode.innerHTML = item.title + '<br>' + 'Адрес: ' + item.address
            itemNode.dataset.cord = item.cord
            container.append(itemNode)
        })

        this.containerInfo.append(this.template)
        window.openCheck = true
        this.addEvent()
        document.querySelector(`[data-id="${this.admin.type}"]`)?.classList.add('check_category')
    }

    addEvent(){
        this.template.querySelectorAll('.category_item').forEach(el=>el.addEventListener('click',this.event.bind(this,el)))
    }

    event(el){
        const cord = JSON.parse(el.dataset.cord)
        this.admin.addPosition.setCordMap([cord[1],cord[0]])
    }

    close(){
        this.containerInfo.innerHTML = ''
        this.containerMap.style.width = "100%"
        this.containerInfo.style.width = "0px"
        this.containerInfo.style.marginRight = "0px"
        window.openCheck = false
    }
}

export default MarkerAll
