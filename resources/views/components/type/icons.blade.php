<div class="tab-pane fide" id="{{ \Map\Enums\MenuEnums::ICONS->value }}" role="tabpanel">
    <div class="row">
        <div class="col-lg-8">
            @foreach($icons as $icon)
                <div class="container_icon delete-element"
                     style="position: relative;display: inline-block;padding: 10px">
                    <a href="{{ route('admin.admin.icon.destroy',$icon) }}" class="delete"
                       style="position: absolute; top: 0px;right: 0px;cursor: pointer"><i class="fas fa-times"
                                                                                          style="color: red"></i></a>
                    <img src="{{ $icon->url }}">
                </div>
            @endforeach
        </div>
        <div class="col-lg-4">
            <form action="{{ route('admin.admin.icon.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <h4>Добавить иконку</h4>
                <div class="form-group">
                    <small>Размер иконки 27 х 32рх</small>
                    <input id="thumbnail" class="form-control" type="file" name="icon">
                </div>
                <input type="submit" class="btn btn-sm btn-success" value="Сохранить">
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('assets/js/admin/delete.js') }}"></script>
