import {ModalWindow} from "../../../../public/assets/js/admin/page/Data/modal/newModalWindow";


class Modal{
    constructor(template) {
        this.template = template
        this.modal = new ModalWindow(document.getElementById('modaldemo8'), true)
        this.btn = this.template.querySelector('.tinymce')
        this.textarea = this.template.querySelector('#description')
        this.addEvent()
    }

    addEvent(){
        this.btn.addEventListener('click',this.openModal.bind(this))
    }

    openModal(){
        this.modal.modal_node.querySelector('.modal-dialog').classList.add('modal-xl');
        const texteria = document.createElement('textarea')
        texteria.classList.add('my-editor');
        this.modal.insertTitle('Описание');
        this.modal.insertNodeBody(texteria);
        tinymce.init(editor_config)
        this.modal.openModal()
        this.modal.opened_modal = () => {
            // this.modal.modal_node.querySelector('iframe').contentDocument.querySelector('.mce-content-body').innerHTML = this.textarea.value
            tinymce.activeEditor.setContent(this.textarea.value)
        }


        this.modal.save_modal = this.save.bind(this)
    }

    save(){
        this.textarea.value = tinymce.activeEditor.getContent()
        this.modal.closeModal()
    }
}

export default Modal
