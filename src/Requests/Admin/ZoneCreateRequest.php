<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ZoneCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return config('admin.request.zone');
    }

    protected function prepareForValidation()
    {
        if($this->cords)
            $this->merge([
                'cords' => json_encode($this->cords)
            ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
