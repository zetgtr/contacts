class Type {
    constructor(map,admin) {
        this.map = map
        this.containerInfo = document.querySelector('.container_info')
        this.containerMap = document.querySelector('.container_map')
        this.admin = admin
    }

    open(addPosition){
        this.template = document.querySelector('#type_template').content.children[0].cloneNode(true)
        this.template.querySelector('.btn-close').addEventListener('click',this.close.bind(this))
        const withMap = this.containerMap.offsetWidth
        if(!window.openCheck){
            this.containerMap.style.width = (withMap - 300) + "px"
            this.containerInfo.style.width = "300px"
            this.containerInfo.style.marginRight = "20px"
        }
        this.containerInfo.innerHTML = ''
        this.containerInfo.append(this.template)
        window.openCheck = true
        this.addEvent()
        document.querySelector(`[data-id="${this.admin.type}"]`)?.classList.add('check_category')
    }

    addEvent(){
        this.template.querySelectorAll('.category_item').forEach(el=>el.addEventListener('click',this.event.bind(this,el)))
    }

    event(el){
        document.querySelector('.check_category')?.classList.remove('check_category')
        if(this.admin.type !== el.dataset.id) {
            this.admin.type = el.dataset.id
            el.classList.add('check_category')
        } else {
            this.admin.type = 'all'
        }
        this.admin.setMarker()
    }

    close(){
        this.containerInfo.innerHTML = ''
        this.containerMap.style.width = "100%"
        this.containerInfo.style.width = "0px"
        this.containerInfo.style.marginRight = "0px"
        window.openCheck = false
    }
}

export default Type
