<template id="marker_template">
    <div>
        <h3 class="card-title"><span>Точки на карте</span><button type="button" class="btn-close"><i class="fas fa-times"></i></button></h3>
        <hr>
        <div class="form-control container_marker_all" style="padding: 0;overflow: auto;max-height: 535px">

        </div>
    </div>
</template>
