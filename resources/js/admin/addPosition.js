import TypeAddress from "./typeAddress.js";
import Marker from "./marker.js";
import Modal from "./modal.js";

export class AddPosition {
    constructor(Admin) {
        this.admin = Admin
        this.containerInfo = document.querySelector('.container_info')
        this.containerMap = document.querySelector('.container_map')
        this.markersNode = document.querySelector('#markers')
        this.address = ''
        this.title = ''
        this.category_id = ''
        this.type_id = ''
        this.description = ''
        this.markers = []
        this.cord = []
        this.marker = new Marker(this)
        // setInterval(() => {
        //     this.createClusterer();
        // }, 100);
    }

    open(id){
        if(!window.openCheck){
            this.address = ''
            this.title = ''
            this.description = ''
            this.category_id = ''
            this.type_id = ''
        }
        if(this.admin?.zone?.listener)
            this.admin.map.removeChild(this.admin.zone.listener)
        this.template = document.getElementById('add_position').content.children[0].cloneNode(true)
        this.template.querySelector('.btn-close').addEventListener('click',this.close.bind(this))
        const withMap = this.containerMap.offsetWidth
        if(!window.openCheck){
            this.containerMap.style.width = (withMap - 300) + "px"
            this.containerInfo.style.width = "300px"
            this.containerInfo.style.marginRight = "20px"
        }
        new Modal(this.template)
        const categoryNode = this.template.querySelector('select[name=category]')
        const typeNode = this.template.querySelector('select[name=types]')
        if(id > 0){
            const data = JSON.parse(this.markersNode.value)
            const item = data.filter(el=>el.id === id)[0]
            console.log(item)
            this.template.querySelector('input[name=header]').value = item.title
            const addressNode = this.template.querySelector('textarea[name=address]')

            const descriptionNode = this.template.querySelector('textarea[name=description]')
            addressNode.value = item.address
            descriptionNode.value = item.description
            categoryNode.value = item.category_id ?? 0
            typeNode.value = item.type_id ?? 0
            addressNode.dataset.cord = item.cord
        }else if(this.address !== ''){
            this.template.querySelector('input[name=header]').value = this.title
            const addressNode = this.template.querySelector('textarea[name=address]')
            const descriptionNode = this.template.querySelector('textarea[name=description]')
            addressNode.value = this.address
            descriptionNode.value = this.description
            addressNode.dataset.cord = this.cord
            console.log(this.category_id)
            categoryNode.value = this.category_id > 0 ? this.category_id : 0
            typeNode.value = this.type_id > 0 ? this.type_id : 0
        }

        this.containerInfo.innerHTML = ''
        this.containerInfo.append(this.template)
        window.openCheck = true
        this.addEvent(id)
    }

    close(){
        this.containerInfo.innerHTML = ''
        this.containerMap.style.width = "100%"
        this.containerInfo.style.width = "0px"
        this.containerInfo.style.marginRight = "0px"
        this.markers = this.markers.filter(el=>el.id)
        document.querySelectorAll('.delete_marker').forEach(el=>el.remove())
        window.openCheck = false
    }

    setDescription(description,id){
        let dataMarkers = JSON.parse(this.markersNode.value)
        const item = dataMarkers.filter(el=>el.id === id)[0]
        if(item){
            dataMarkers = dataMarkers.filter(el=>el.id !== id)
            item.description = description.value
            dataMarkers.push(item)
            this.markersNode.value = JSON.stringify(dataMarkers)
        }
        this.description = description.value
    }

    addEvent(id){
        const address = this.template.querySelector('textarea[name=address]')
        const description = this.template.querySelector('textarea[name=description]')
        const category = this.template.querySelector('select[name=category]')
        new TypeAddress(this.template,id?this.el:null,this)
        category.addEventListener('input',()=>{
            this.category_id = category.value
        })
        const title = this.template.querySelector('input[name=header]')
        const save = this.template.querySelector('.btn-success')
        const remove = this.template.querySelector('.modal-danger')
        address.addEventListener('input',this.setAddress.bind(this,address,id))
        description.addEventListener('input',this.setDescription.bind(this,description,id))
        title.addEventListener('input',()=>{
            let dataMarkers = JSON.parse(this.markersNode.value)
            const item = dataMarkers.filter(el=>el.id === id)[0]
            if(item){
                dataMarkers = dataMarkers.filter(el=>el.id !== id)
                item.title = title.value
                dataMarkers.push(item)
                this.markersNode.value = JSON.stringify(dataMarkers)
            }
            this.title = title.value
        })
        if(id > 0){
            this.template.querySelector('.card-title span').innerText = 'Редактирование точки'
            remove.classList.remove('d-none')
            document.querySelectorAll('.delete_marker').forEach(el=>el.remove())
            save.innerText = 'Редактировать'
            save.addEventListener('click',this.edit.bind(this,id))
            remove.addEventListener('click',this.remove.bind(this,id))
        }else {
            remove.classList.add('d-none')
            this.template.querySelector('.card-title span').innerText = 'Добавление точки'
            save.innerText = 'Добавить'
            save.addEventListener('click',this.save.bind(this))
        }
    }

    remove(id){
        axios.delete('/admin/map/marker/'+id).then(()=>{
            document.querySelector('.marker_'+id).remove()
            this.close()
        })
    }

    edit(id){
        const header = this.template.querySelector('input[name=header]').value
        const addressNode = this.template.querySelector('textarea[name=address]')
        const description = this.template.querySelector('textarea[name=description]').value
        const types = this.template.querySelector('#types').value
        const category = this.template.querySelector('select[name=category]').value
        axios.post('/admin/map/marker/'+id,{
            title: header,
            description: description,
            type_id: types,
            category_id: category,
            cord: addressNode.dataset.cord,
            address: addressNode.value
        }).then(({data})=>{
            console.log(data)
            this.close()
        })
    }

    setAddress(address,id){
        console.log( this.ids)
        console.log(this.containerAddress)
        this.containerAddress = this.template.querySelector('.container_address')
        this.containerAddress.innerHTML = ""
        this.containerAddress.classList.add('address_check')
        this.address = address.value
        ymaps.geocode(address.value, { results: 4 }).then((res)=>{
            const geoObjects = res.geoObjects.toArray();
            geoObjects.forEach((geoObject, index) => {
                const addressTemplate = document.getElementById('address_template').content.children[0].cloneNode(true)
                const addressLine = geoObject.getAddressLine();
                const coordinates = geoObject.geometry.getCoordinates();
                const border = document.createElement('div')
                border.style.borderTop = '1px solid #e9edf4'
                addressTemplate.querySelector('span').innerText = addressLine
                console.log(id)
                addressTemplate.addEventListener('click',this.setEventAddress.bind(this,address,addressLine,coordinates,id))
                this.containerAddress.append(border)
                this.containerAddress.append(addressTemplate)
            });
        })
    }
    setEventAddress(address,addressLine,coordinates,id){
        address.value = addressLine
        this.address = addressLine
        this.cord = [coordinates[1],coordinates[0]]
        address.dataset.cord = JSON.stringify([coordinates[1],coordinates[0]])
        this.setCordMap(coordinates)
        this.setMarker([coordinates[1],coordinates[0]],this.setMarkerNode(id),id)


        this.containerAddress.innerHTML = ''
        this.containerAddress.classList.remove('address_check')
    }

    setCordMap(cord){
        console.log(cord)
        const newCenter = {
            center: [cord[1],cord[0]], // [lng, lat]
            zoom: 16.6
        };
        const newTilt = (45 * Math.PI) / 180;
        this.admin.map.update({location: {...newCenter, duration: 1000}, newTilt})
    }

    async createClusterer() {
        const {YMapClusterer, clusterByGrid} = await ymaps3.import('@yandex/ymaps3-clusterer@0.0.1');
        if(this.clusterer){
            this.admin.map.removeChild(this.clusterer)
        }
        this.clusterer = new YMapClusterer({
            method: clusterByGrid({ gridSize: 50 }),
            features: this.markers,
            marker: this.marker.marker.bind(this.marker),
            cluster: this.marker.cluster,
        });

        this.admin.map.addChild(this.clusterer);
    }

    async setMarker(cord,markerElement,id){
        await ymaps3.ready;
        await this.admin.map;

        if (!id) {
            document.querySelectorAll('.delete_marker').forEach(el => el.remove());
        }

        const container = document.createElement('div');
        this.el = container;
        container.className = id ? 'container_marker item_map marker_' + id : 'container_marker item_map delete_marker';
        let dataMarkers = JSON.parse(this.markersNode.value)
        const item = dataMarkers.filter(el=>el.id === id)[0]
        if(item)
            container.title = item.title
        container.style.cursor = 'pointer';
        container.append(markerElement);

        let check = false
        this.markers.forEach((el,key)=>{
            if(el.id === id)  {
                this.markers[key].geometry.coordinates = cord
                check = true
            }
        })

        const feature = {
            type: 'Feature',
            id: id,
            geometry: {
                coordinates: cord,
            },
            container
        };

        if(!check)
            this.markers.push(feature);

        this.createClusterer();
    }

    save(){
        const header = this.template.querySelector('input[name=header]').value
        const description = this.template.querySelector('textarea[name=description]').value
        const addressNode = this.template.querySelector('textarea[name=address]')
        const type = this.template.querySelector('#types').value
        const category = this.template.querySelector('select[name=category]').value
        axios.post('/admin/map',{
            title: header,
            description: description,
            cord: addressNode.dataset.cord,
            type_id: type,
            category_id: category,
            address: addressNode.value
        }).then(({data})=>{
            if(data.status === true){
                const dataMarkers = JSON.parse(this.markersNode.value)
                dataMarkers.push(data.data)
                this.markersNode.value = JSON.stringify(dataMarkers)
                document.querySelectorAll('.delete_marker').forEach(el=>el.remove())
                this.setMarker(JSON.parse(data.data.cord),this.setMarkerNode(data.data.id),data.data.id)
                this.close()
            }
        })
    }

    setMarkerNode(id){
        let dataMarkers = JSON.parse(this.markersNode.value)
        const item = dataMarkers.filter(el=>el.id === id)[0]
        let markerElement
        if (item?.type?.icons?.url) {
            markerElement = document.createElement('div')
            const img = document.createElement('img')
            img.src = item.type?.icons?.url
            markerElement.classList.add('marker')
            markerElement.classList.add('marker_'+id)
            markerElement.append(img)
        }
        else {
            markerElement = document.createElement('i');
            markerElement.className = 'fas fa-admin-marker-alt marker_default';
        }

        return markerElement
    }
}
