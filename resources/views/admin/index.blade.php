@extends('layouts.admin')
@section('title',config('contact.title'))
@section('content')
    <div class="card">
        <div class="card-body">
            <x-warning />
            1
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.contact")}}">{{ config('contact.title') }}</a></li>
        </ol>
    </div>
@endsection
