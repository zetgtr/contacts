import {Map} from "./Map";
import {AddPosition} from './addPosition';
import Category from "./category.js";
import Type from "./type.js";
import MarkerAll from "./marker_all.js";
import Zone from "./zone/zone";
class AdminMap extends Map{
    async init(){
        await ymaps3.ready;
        window.openCheck = false
        this.addPosition = new AddPosition(this)
        this.categoryClass = new Category(this.map,this)
        this.typeClass = new Type(this.map,this)
        this.markerAll = new MarkerAll(this.map,this)
        this.zone = new Zone(this)
        this.category = 'all'
        this.type = 'all'
        this.setMarker()
        this.addBtnEvent()
    }

    addBtnEvent(){
        this.addBtn('Добавить точку','top left',this.addPosition.open.bind(this.addPosition,null))
        this.addBtn('Категории','top left',this.categoryClass.open.bind(this.categoryClass,this.addPosition)).then(()=>this.getBtn('Категории','category_map_btn'))
        this.addBtn('Типы','top left',this.typeClass.open.bind(this.typeClass,this.addPosition)).then(()=>this.getBtn('Типы','type_map_btn'))
        this.addBtn('Точки на карте','bottom left',this.markerAll.open.bind(this.markerAll))
        this.addBtn('Добавить зону','top right',this.zone.open.bind(this.zone)).then(()=>this.getBtn('Добавить зону','zone_map_btn'))
    }

    async setMarker(){
        await ymaps3.ready;
        const data = JSON.parse(document.getElementById('markers').value)
        this.addPosition.markers.forEach(el=>{
            el.container.classList.add('d-none')
        })
        if(this.admin?.zone?.listener)
            this.admin.map.removeChild(this.admin.zone.listener)
        this.addPosition.markers = []
        data.forEach(el=>{
            if((el.category_id == this.category || this.category == 'all') && (el.type_id == this.type || this.type == 'all')) {
                console.log(el.category_id,this.category)
                let markerElement;
                if (el.type?.icons?.url) {
                    markerElement = document.createElement('div')
                    const img = document.createElement('img')
                    img.src = el.type?.icons?.url
                    markerElement.classList.add('marker')
                    markerElement.append(img)
                } else {
                    markerElement = document.createElement('i');
                    markerElement.className = 'fas fa-admin-marker-alt marker_default';
                }

                this.addPosition.setMarker(JSON.parse(el.cord), markerElement, el.id)
            }
        })
    }

    async addBtn(text,position,fun){
        await ymaps3.ready;

        const {YMapControls, YMapControlButton} = ymaps3;

        const controls = new YMapControls({position,orientation: 'horizontal'});

        const button = new YMapControlButton({
            text,
            onClick: fun
        });
        // button.className = className
        controls.addChild(button);
        this.map.addChild(controls);
    }

    getBtn(text,className){
        const btn = this.findElementByText(text)
        btn.closest('.ymaps3x0--controls').classList.add(className)
    }
    findElementByText(text) {
        const buttons = document.querySelectorAll('.ymaps3x0--button.ymaps3x0--control-button');

        for (const button of buttons) {
            if (button.textContent.includes(text)) {
                return button;
            }
        }
        return null;
    }

    close(){
        this.containerInfo.innerHTML = ''
        this.containerMap.style.width = "100%"
        this.containerInfo.style.width = "0px"
        this.containerInfo.style.marginRight = "0px"
        window.openCheck = false
        this.admin.map.removeChild(this.listener)
        this.setDataPolygons()
    }
}

$(document).ready(()=>{
    const adminMap = new AdminMap
    adminMap.init()
})
