<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MapSettingsRequest extends FormRequest
{
    public function rules(): array
    {
        return config('admin.request.settings');
    }

    public function authorize(): bool
    {
        return true;
    }
}
