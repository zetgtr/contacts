@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <div class="card-header">
            <h3 class="card-title">Редактировать категорию</h3>
        </div>
        <x-admin.navigatin-js :links="$navigation" />
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.catalog.category.update',['category'=>$category]) }}" enctype="multipart/form-data" method="post" class="row row-page-create">
                @csrf
                @method('PUT')
                <input type="hidden" value="{{$category->id}}" name="id">
                <div class="tab-content">
                    <x-catalog::edit.content :category="$category" />
                    <x-catalog::edit.seo :category="$category" />
                    <x-catalog::edit.photo :category="$category" />
                </div>
                <div>
                    <input type="submit" value="Сохранить" class="btn btn-success btn-sm">
                </div>
            </form>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog")}}">Каталог</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.catalog.category.index")}}">Список категорий</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать категорию</li>
        </ol>
    </div>
@endsection