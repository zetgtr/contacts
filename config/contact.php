<?php

use Catalog\Enums\CatalogEnums;
use Laravel\Fortify\Features;
use Quest\Enums\MenuEnums;

return [
    'database_id' => 977,
    'title' => 'Контакты',
    'fillable' => [
        'quest' => [
            'title','quest','order'
        ],
    ],
    'request' => [

    ],
];
