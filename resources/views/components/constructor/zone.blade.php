<template id="zone_template">
    <div>
        <h3 class="card-title"><span>Зоны</span>
            <button type="button" class="btn-close"><i class="fas fa-times"></i></button>
        </h3>
        <hr>
        <div class="form-group">
            <label>Заголовок</label>
            <input type="text" class="form-control" name="title">
        </div>
        <div class="form-group">
            <label>Описание - <a class="btn btn-sm edit-role modal-effect btn-primary tinymce badge">tinymce</a> </label>
            <textarea type="text" id="description" name="description" autocomplete="off"
                      class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label>Толщина границы</label>
            <input type="number" id="color_border" name="width_border" value="4" autocomplete="off"
                   class="form-control"></input>
        </div>
        <div class="form-group">
            <label>Цвет границы</label>
            <input type="color" id="color_border" name="color_border" value="#3838DB" autocomplete="off"
                   class="form-control"></input>
        </div>
        <div class="form-group">
            <label>Цвет заливки</label>
            <input type="color" id="color_fill" name="color_fill" value="#45c1fd" autocomplete="off"
                   class="form-control"></input>
        </div>
        <input type="submit" class="btn btn-success btn-sm" value="Сохранить">
        <input type="submit" class="btn d-none btn-danger btn-sm" value="Удалить">
    </div>
</template>

<template id="marker_zone">
    <div class="map-placemark" style="cursor: pointer;">
        <div dir="ltr" aria-hidden="true" class="map-placemark__wrapper">
            <div class="search-placemark-view _position_right" style="margin-top: -11px; margin-left: -11px;">
                <div class="search-placemark-view__icon" style="width: 22px; height: 22px;">
                    <div class="search-placemark-icon _mode_dot _group-mode_default _state_default">
                        <div style="width: 22px; height: 22px;position: relative">
                            <div class="count_marker_zone" style="position: absolute;left: 8px;font-size: 10px;color: #fff;top: 3.3px;"></div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                <defs>
                                    <path id="2ccc4e9f8fe2f8acc9e7eab53f797f56__b"
                                          d="M7 14A7 7 0 1 0 7 0a7 7 0 0 0 0 14z"></path>
                                    <filter id="2ccc4e9f8fe2f8acc9e7eab53f797f56__a" width="150%" height="150%" x="-25%"
                                            y="-17.9%" filterUnits="objectBoundingBox">
                                        <feOffset dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                                        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1"
                                                        result="shadowBlurOuter1"></feGaussianBlur>
                                        <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"
                                                       in="shadowBlurOuter1"></feColorMatrix>
                                    </filter>
                                </defs>

                                <g fill="none" fill-rule="evenodd" transform="translate(4 4)">
                                    <use fill="#000" filter="url(#2ccc4e9f8fe2f8acc9e7eab53f797f56__a)"
                                         xlink:href="#2ccc4e9f8fe2f8acc9e7eab53f797f56__b"></use>
                                    <use fill="#FFF" xlink:href="#2ccc4e9f8fe2f8acc9e7eab53f797f56__b"></use>
                                    <path d="M7 12A5 5 0 1 1 7 2a5 5 0 0 1 0 10z"  fill="currentColor"></path>
                                    <path fill="#000" class="tint" style="fill: #0b5ed7" d="M7 12A5 5 0 1 1 7 2a5 5 0 0 1 0 10z"></path>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
