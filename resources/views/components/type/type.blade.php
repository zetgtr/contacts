<style>
    .icon_type {
        padding: 6px 8px;
        cursor: pointer
    }

    .icon_type:hover {
        padding: 5px 7px !important;
        border: 1px solid #e7e4e4;
        border-radius: 5px;
    }

    .border-red {
        padding: 5px 7px !important;
        border: 1px solid #e7e4e4;
        background-color: #e8fafd;
        border-radius: 5px;
    }
</style>
<div class="tab-pane active fide" id="{{ \Map\Enums\MenuEnums::TYPE->value }}" role="tabpanel">
    <div class="row">
        <div class="col-lg-8">
            <ul class="list-group">
                @foreach($types as $type)
                    <li class="list-group-item delete-element" data-id="2" data-name="Файлы" data-position="left">
                        <div class="">
                            <img style="width: 18px;margin-right: 10px" src="{{ $type->icons->url }}">

                            <span>{{ $type->title }}</span>
                        </div>
                        <div class="dnd-edit" style="top: 0">
                            <a href="{{ route('admin.admin.type.destroy', $type) }}" style="color: #fff"
                               class="button-delete delete btn btn-danger btn-xs pull-right" data-owner-id="1">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                            <a href="{{ route('admin.admin.type.edit', $type) }}" style="color: #fff"
                               class="button-edit btn btn-success btn-xs pull-right" data-owner-id="1">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-lg-4">
            <form id="form_type" action="{{ route('admin.admin.type.store') }}" method="POST">
                @csrf
                <h4>Добавить тип</h4>
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="form-group">
                    <label>Иконка</label>
                    <input type="hidden" name="icon">
                    <div class="form-control">
                        @foreach($icons as $icon)
                            <img id="icon_{{$icon->id}}" class="icon_type" data-id="{{$icon->id}}"
                                 src="{{ $icon->url }}">
                        @endforeach
                    </div>
                </div>
                <input type="submit" class="btn btn-sm btn-success" value="Добавить">
                <input type="submit" class="btn btn-sm btn-danger d-none" value="Отменить">
            </form>
        </div>
    </div>
</div>
@method('PUT')
<script>
    document.querySelectorAll('.icon_type').forEach(el => {
        el.addEventListener('click', () => {
            document.querySelectorAll('.icon_type').forEach(el => el.classList.remove('border-red'));
            const id = el.dataset.id
            el.classList.add('border-red')
            document.querySelector('input[name=icon]').value = id
        })
    })
    document.querySelectorAll('.button-edit').forEach(el => {
        el.addEventListener('click', (e) => {
            e.preventDefault();
            axios.get(el.href).then(({data}) => {
                document.querySelector('#form_type').append(document.querySelector('input[name=_method]').cloneNode(true))
                document.querySelectorAll('.icon_type').forEach(el => el.classList.remove('border-red'));
                document.querySelector('#form_type').action = '/admin/admin/type/' + data.type.id
                document.querySelector('#form_type h4').innerText = 'Редактировать тип'
                document.querySelector('#form_type').querySelector('input[name=title]').value = data.type.title
                document.querySelector('#form_type').querySelector('input[name=icon]').value = data.type.icon
                document.getElementById('icon_' + data.type.icon).classList.add('border-red')
                document.querySelector('#form_type .btn-success').value = 'Обновить'
                document.querySelector('#form_type').querySelector('.btn-danger').classList.remove('d-none');
            })
        })
    })
    document.querySelector('#form_type').querySelector('.btn-danger').addEventListener('click', (e) => {
        e.preventDefault()
        document.querySelector('#form_type input[name=_method]').remove()
        document.querySelectorAll('.icon_type').forEach(el => el.classList.remove('border-red'));
        document.querySelector('#form_type').action = '/admin/admin/type'
        document.querySelector('#form_type h4').innerText = 'Добавить тип'
        document.querySelector('#form_type .btn-success').value = 'Добавить'
        document.querySelector('#form_type').querySelector('input[name=title]').value = ""
        document.querySelector('#form_type').querySelector('input[name=icon]').value = ""
        document.querySelector('#form_type').querySelector('.btn-danger').classList.add('d-none')
    })
</script>
