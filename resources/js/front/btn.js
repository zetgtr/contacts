class Btn {
    constructor(Front) {
        this.front = Front
        this.setBtn()
    }

    setBtn(){
        this.addBtn('Категории','top left',this.front.category.open.bind(this.front.category))
        this.addBtn('Типы','top left',this.front.type.open.bind(this.front.type)).then(()=>this.getBtn('Типы','type_map_btn'))
        this.addBtn('Точки на карте','bottom left',this.front.markerAll.open.bind(this.front.markerAll))
    }

    async addBtn(text,position,fun){
        const {YMapControls, YMapControlButton} = ymaps3;

        const controls = new YMapControls({position,orientation: 'horizontal'});

        const button = new YMapControlButton({
            text,
            onClick: fun
        });
        // button.className = className
        controls.addChild(button);
        this.front.map.addChild(controls);
    }

    getBtn(text,className){
        const btn = this.findElementByText(text)
        btn.closest('.ymaps3x0--controls').classList.add(className)
    }
    findElementByText(text) {
        const buttons = document.querySelectorAll('.ymaps3x0--button.ymaps3x0--control-button');

        for (const button of buttons) {
            if (button.textContent.includes(text)) {
                return button;
            }
        }
        return null;
    }
}

export default Btn
