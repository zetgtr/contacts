import HintWindow from '../admin/zone/Hint'

class Zone {
    constructor(Front) {
        this.front = Front
        this.dataNode = document.querySelector('#zone_data')
        this.cord = [[]]
        this.color = '#006efc'
        this.fill = '#3838DB'
        this.width = 4
        this.id = 0
        this.polygon = null
        this.polygons = []
        this.markers = []
        this.setData()
    }
    async setData(){
        const {YMapHint} = await ymaps3.import('@yandex/ymaps3-hint@0.0.1');
        const hint = new YMapHint({hint: (object) => object?.properties?.hint});
        hint.addChild(new HintWindow())
        this.front.map.addChild(hint);
        JSON.parse(this.dataNode.value).forEach(el=>{
            this.set(el,JSON.parse(el.cords))
        })
    }
    set(el,cords){
        this.fill = el.fill
        this.markers = el.markers ?? []
        this.title = el.title
        this.description = el.description
        this.polygon = el.polygon
        this.color = el.color
        this.width = el.width
        this.cord = cords
        this.id = el.id

        this.setPolygon()
    }
    setPolygon(){
        const {YMapFeature} = ymaps3;

        const hintTemplate = document.getElementById('hint_template').content.children[0].cloneNode(true)
        hintTemplate.querySelector('.title_hint').innerText = this.title
        hintTemplate.querySelector('.desc_hint').innerHTML = this.description

        const polygon = new YMapFeature({
            geometry: {
                type: 'Polygon',
                coordinates: this.cord,

            },
            properties: {
                hint: hintTemplate.outerHTML
            },
            onClick: this.click.bind(this),
            style: {stroke: [{color: this.color, width: this.width}], fill: this.fill}
        });
        const dataPolygon = {
            title: this.title,
            color: this.color,
            description: this.description,
            width: this.width,
            fill: this.fill,
            id: this.id,
            cord: this.cord,
            markers: this.markers,
            polygon
        }
        this.polygons = this.polygons.filter(el=>el.id !== this.id)
        this.polygons.push(dataPolygon)
        if(this.polygon)
            this.front.map.removeChild(this.polygon);
        this.polygon = polygon
        this.front.map.addChild(polygon);
    }

    click(e){
        console.log(e)
    }


    pointInPolygon(point, polygon) {
        const x = point[0], y = point[1];
        let inside = false;
        if(polygon[0]){
            polygon.push(polygon[0])
            for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
                const xi = polygon[i][0], yi = polygon[i][1];
                const xj = polygon[j][0], yj = polygon[j][1];

                const intersect = ((yi > y) != (yj > y)) &&
                    (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }
        }
        return inside;
    }

    getPolygonCord(point){
        return this.polygons.filter(el=>{
            if(el.cord[0].length > 0)
                return this.pointInPolygon(point, el.cord[0])
            return false
        })
    }

}

export default Zone
