<template id="category_template">
    <div>
        <h3 class="card-title"><span>Категории</span><button type="button" class="btn-close"><i class="fas fa-times"></i></button></h3>
        <hr>
        <div class="form-control" style="padding: 0;overflow: auto;max-height: 535px">
            @foreach($categories as $category)
                <div class="category_item" data-id="{{ $category->id }}">
                    {{$category->title}}
                </div>
            @endforeach
        </div>
    </div>
</template>
