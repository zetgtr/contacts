<?php

namespace Map\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Map\Models\MapSettings;

class SettingsMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if(MapSettings::first()){
            return $next($request);
        }

        return redirect()->route('admin.admin.settings.index')->with('error','Необходимо заполнить настройки');
    }
}
