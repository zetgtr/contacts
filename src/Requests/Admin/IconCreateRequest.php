<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;

class IconCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return config('admin.request.icon');
    }

    protected function prepareForValidation()
    {
        if ($this->icon) {
            $image = new \Imagick($this->icon->getRealPath());

            $image->setImageFormat('webp');

            $image->setImageCompressionQuality(70);

            $directory = public_path('storage/admin/icons');

            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $fileName = 'admin/icons/' . uniqid() . '.webp';

            $image->writeImage(public_path('storage/' . $fileName));

            $this->merge([
                'url' => "/storage/".$fileName
            ]);
        }

    }

    public function authorize(): bool
    {
        return true;
    }
}
