class Marker {
    constructor(Front) {
        this.fornt = Front
        this.category_id = 'all'
        this.type_id = 'all'
        this.markers = []
        this.setMarker()
    }

    async setMarker(){
        await ymaps3.ready;
        this.markersNode = document.getElementById('markers')
        this.markers.forEach(el=>{
            el.container.classList.add('d-none')
        })
        this.markers = []
        JSON.parse(this.markersNode.value).forEach(el=>{
            if((el.category_id == this.category_id || this.category_id == 'all') && (el.type_id == this.type_id || this.type_id == 'all')) {
                let markerElement;
                if (el.type?.icons?.url) {
                    markerElement = document.createElement('div')
                    const img = document.createElement('img')
                    img.src = el.type?.icons?.url
                    markerElement.classList.add('marker')
                    markerElement.append(img)
                } else {
                    markerElement = document.createElement('i');
                    markerElement.className = 'fas fa-admin-marker-alt marker_default';
                }
                const markerInfo = document.createElement("div");
                const markerTitle = document.createElement("div");
                const markerDesc = document.createElement("div");
                const markerCloseBtn = document.createElement("button");
                const markerCloseIcon = document.createElement("i");

                markerInfo.className = "marker__info d-none";
                markerTitle.className = "marker__title";
                markerDesc.className = "marker__desc";
                markerCloseBtn.className = "marker__close-button";
                markerCloseIcon.className = "fas fa-times";

                markerTitle.innerHTML = el.title;
                markerDesc.innerHTML = el.description;

                markerCloseBtn.append(markerCloseIcon);
                markerInfo.append(markerTitle, markerDesc, markerCloseBtn);


                this.markerData(JSON.parse(el.cord), markerElement,markerInfo, el.id)
            }
        })
    }

    async markerData(cord,markerElement,markerInfo,id){
        await ymaps3.ready;
        console.log(this.fornt.zone.getPolygonCord(cord))
        const markerContainer = document.createElement("div");
        markerContainer.className = "marker__container";
        const container = document.createElement('div');
        this.el = container;
        container.className = 'container_marker item_map marker_' + id
        let dataMarkers = JSON.parse(this.markersNode.value)
        const item = dataMarkers.filter(el=>el.id === id)[0]
        if(item)
            container.title = item.title
        container.style.cursor = 'pointer';
        container.append(markerElement);
        markerContainer.append(container, markerInfo);
        const feature = {
            type: 'Feature',
            id: id,
            geometry: {
                coordinates: cord,
            },
            item,
            container : markerContainer
        };

        this.markers.push(feature);
        console.log(this.markers)
        this.createClusterer();
    }

    async createClusterer() {
        console.log(1)
        const {YMapClusterer, clusterByGrid} = await ymaps3.import('@yandex/ymaps3-clusterer@0.0.1');
        if(this.clusterer){
            this.fornt.map.removeChild(this.clusterer)
        }
        this.clusterer = new YMapClusterer({
            method: clusterByGrid({ gridSize: 50 }),
            features: this.markers,
            marker: this.marker.bind(this),
            cluster: this.cluster,
        });

        this.fornt.info.setInfo()
        this.fornt.map.addChild(this.clusterer);
    }

    marker = (feature) => {
        const { YMapMarker } = ymaps3;
        return new YMapMarker(
            {
                coordinates: feature.geometry.coordinates,
                onClick: this.fornt.info.openInfo.bind(this,feature.container)
            },
            feature.container
        )
    }

    setCordMap(cord){
        const newCenter = {
            center: [cord[1],cord[0]], // [lng, lat]
            zoom: 16.6
        };
        const newTilt = (45 * Math.PI) / 180;
        this.fornt.map.update({location: {...newCenter, duration: 1000}, newTilt})
    }

    cluster = (coordinates,features) => {
        const { YMapMarker } = ymaps3;
        const data = {
            class: this,
            features
        }
        return new YMapMarker(
            {
                coordinates: coordinates,
                onClick: this.openMarker.bind(data)
            },
            this.circle(features.length)
        )
    }

    openMarker(e){
        this.features.forEach(feature=>{
            console.log(feature)
            const data = {...feature}
            data.container.classList.add('remove')
            const marker = this.class.marker(feature)
            this.class.fornt.map.addChild(marker)
        })
        console.log(e.srcElement.closest('.ymaps3x0--marker'))
        setTimeout(()=>{
            e.srcElement.closest('.ymaps3x0--marker').remove()
        },100)

    }

    circle(count) {
        const circle = document.createElement('div');
        circle.classList.add('circle');
        circle.innerHTML = `
                  <div class="circle-content item_map" style="cursor: pointer">
                      <span class="circle-text">${count}</span>
                  </div>
              `;
        return circle;
    }
}

export default Marker
