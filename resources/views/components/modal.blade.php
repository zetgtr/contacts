<div class="modal fade effect-fall" id="my_modal">
    <div class="modal-dialog modal-dialog-centered text-center" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="modal-warning">

                </div>
                <div class="modal-wrapper"></div>
            </div>
            <div class="modal-footer">
                <div class="buttons-extends"></div>
                <button type="button" class="btn btn-sm btn-success modal-save">Добавить</button>
            </div>
        </div>
    </div>
</div>
